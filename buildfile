#!/bin/bash

# load bash builder library
. ${bash_builder_prefix}/library/builder.sh

maintainer "Alexey Fedorov" "waterlink000@gmail.com"

# --- config ---
# environ key value <=> key=value
environ php_version 5.3.21
environ php_url http://ua1.php.net/distributions/php-${php_version}.tar.gz
environ php_folder php-${php_version}
environ php_archive ${php_folder}.tar.gz
environ php_prefix /opt/php
environ php_config --prefix=${php_prefix} --with-config-file-scan-dir=${php_prefix}/etc/conf.d --with-fpm-user=www-data --with-fpm-group=www-data --enable-fpm --enable-soap --with-mcrypt --enable-mbstring --with-openssl --with-kerberos --with-gd --with-jpeg-dir=/usr/lib --enable-gd-native-ttf --with-libxml-dir=/usr/lib --with-curl --enable-zip --enable-sockets --with-zlib --enable-exif --enable-ftp --with-iconv --with-gettext --enable-gd-native-ttf --with-freetype-dir=/usr --with-mysql --enable-dbase

# --- dependencies ---
package-update             # apt-get update for ubuntu
# package-install    <=>  apt-get install -yy -q -f $@;   #  $@ -- all parameters
package-install gcc libpcre++-dev libcurl4-openssl-dev libxml2-dev libjpeg-dev libpng-dev libxpm-dev libfreetype6-dev libmcrypt-dev libmysql++-dev autoconf libevent-dev libltdl-dev

# --- installation ---
download ${php_url} ${php_archive} ${php_folder}
install ${php_config}
clean

# --- configuration ---
# copies files from builder folder to install path
add templates/php-fpm.conf ${php_prefix}/etc/php-fpm.conf
# arbitary run
run mkdir -p ${php_prefix}/etc/conf.d
run echo 'date.timezone="Europe/Kiev"' > ${php_prefix}/etc/conf.d/timezone.ini

# --- extensions ---

# load php build inner pecl library
. library/pecl.sh

# apc
pecl reinstall apc
pecl enable apc

# allow big upload
run echo 'post_max_size = 10M' > ${php_prefix}/etc/conf.d/allow_big_uploads.ini
run echo 'upload_max_size = 10M' >> ${php_prefix}/etc/conf.d/allow_big_uploads.ini

# --- start script ---
add templates/php-fpm.start /etc/init.d/php-fpm
run chmod +x /etc/init.d/php-fpm
run update-rc.d php-fpm defaults

