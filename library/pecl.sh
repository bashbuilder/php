# maintainer "Anton Priadko" "d.ark.3.1415 (at) gmail.com"

# Example usage
# pecl reinstall mongo 1.3.2      # will do ${php_prefix}/bin/pecl uninstall mongo; ${php_prefix}/bin/pecl install mongo-1.3.2
# pecl enable mongo               # will do echo 'extension=mongo.so' > ${php_prefix}/etc/conf.d/mongo.ini


function pecl {
	# usage pecl action package [version]
	action=$1
	name=$2
	full_name=${name}
	if [ $# -gt 2 ]; then
		version=$3
		full_name="${name}-${version}"
	fi

	if [ "${action}" == "reinstall" ]
	then
		${php_prefix}/bin/pecl uninstall ${name}
		printf "\n" | ${php_prefix}/bin/pecl install ${full_name}
	elif [ "${action}" == "enable" ]
	then
		echo "extension=${name}.so" > ${php_prefix}/etc/conf.d/${name}.ini
	elif [ "${action}" == "uninstall" ]
	then
		${php_prefix}/bin/pecl uninstall ${name}
	elif [ "${action}" == "install" ]
	then
		printf "\n" | ${php_prefix}/bin/pecl install ${full_name}
	else
		echo_e "Unknown command"
	fi
}
